# Environment Variables, ConfigMaps and Secrets

## Environment variables and commands
Look into the file `pod-print-something.yaml`. It contains two useful functionalities we didn't see before. Can you name them?
```
$ kubectl apply -f pod-print-something.yaml
pod/print-something created
```

This pod just printed something to stdout and then exited. To view it's output, use `kubectl logs`:

```
$ kubectl logs print-something
```
## Configmaps

Let's create a configmap using the commandline:
```
$ kubectl create configmap nginx-html --from-file=./index.html
configmap/nginx-html created
```

The pod `pod-configmap-mount.yaml` mounts your configmap in the `/usr/share/nginx/html` folder.
```
$ kubectl apply -f pod-configmap-mount.yaml
pod/nginx-html created
```

Let's view its contents:
```
kubectl exec nginx-html -- cat /usr/share/nginx/html/index.html
<html>
<head>
</head>
<body>
<h1>Hello World<h1>
</body>
</html>✔
```
---
**Exercise**


* Find a way to call the pod we just created on port 80 with your webbrowser.
* Change the contents of the ConfigMap `nginx-html` using e.g. `kubectl edit` and call the pod again. What happens?

TIP:  
```
echo a3ViZWN0bCBleHBvc2UgcG9kIG5naW54LWh0bWwgLS1wb3J0PTgwIC0tbmFtZT1teS1uZ2lueC1zZXJ2aWNlIC0tdHlwZT1Mb2FkQmFsYW5jZXIK | base64 -d
```



---

## Secrets

Let's create a secret using the command line
```bash
kubectl create secret generic some-secret --from-literal=username=admin --from-literal=password=1234test
```

Now look into the secret:
```bash
kubectl get secret/some-secret -o yaml
```

The values `username` and `password` are only shown `base64` encoded. Of course, this doesn't
provide any real security.

To view the values, you can pipe them into `base64 --decode`:

```bash
echo YWRtaW4= | base64 -d
```

Next look at the file `pod-with-env-from-secret` and understand how the secret you created is mounted here to provide
environment variables to this pod.

Now apply the manifest file:

```bash
kubectl apply -f pod-with-env-from-secret.yaml
```

List the pod's environment variables.
```bash
kubectl exec secret-demo-pod -- printenv
```
We can see the pod's environment.
```
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=secret-demo-pod
SECRET_PASSWORD=Ash1eesaireM
SECRET_USERNAME=admin
...
```

