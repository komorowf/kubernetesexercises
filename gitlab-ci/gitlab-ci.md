# Gitlab CI exercises

[[_TOC_]]

**Note:** Using the official Gitlab CI Reference Guide will help you with this exercise:
https://docs.gitlab.com/ee/ci/yaml/

## Using Gitlab CI Variables

Start by forking the project `https://gitlab.com/mt-ag-k8s-training-course/springmetricdemo`. It's important to first click `star`, then `fork`. 😉

![img_1.png](img_1.png)

---


```yaml
variables:
  CI_VARIABLE: "ROOT"

stages:
  - test

env-var-job:
  stage: test
  variables:
    CI_VARIABLE: "IN_JOB"
  script:
    - env
    - echo "$CI_VARIABLE"
```
Next make sure to fork the project into `MT K8S Workshop Infrastructure` ![img_3.png](img_3.png) to be able to make use of the gitlab runner processing gitlab ci jobs inside our kubernetes cluster.
then create a `.gitlab-ci.yml` with the above content.

---

![img_4.png](img_4.png)

Now try to set a gitlab ci variable via the gitlab gui with different value and observe the result.

---
Try tagging a commit, creating a branch, or a merge request and observe the changes to Gitlab CI environment variables that are shown
inside the job's logs.

## Creating a pipeline trigger



![img_5.png](img_5.png)
Create a trigger token in `Menu -> Projects -> Settings -> CI/CD -> Pipeline Triggers`.



![img_6.png](img_6.png)
Copy the token and trigger the pipeline using the `curl` command shown after creating the token.
```bash
curl -X POST --fail -F token=<YOUR_TOKEN> -F ref=REF_NAME https://gitlab.com/api/v4/projects/<YOUR_PROJECT_ID>/trigger/pipeline
```

Now try passing the ci variable again using variables [VARIABLE]=VALUE

```bash
curl -X POST --fail -F token=<YOUR_TOKEN> -F ref=REF_NAME https://gitlab.com/api/v4/projects/<YOUR_PROJECT_ID>/trigger/pipeline&[CI_VARIABLE]=pipeline
```

## Using schedules

Finally lets schedule the pipeline to run regularily using the _Schedules_ function

![img.png](img.png)

### Exercise 1

![img_2.png](img_2.png)

Configure you pipeline to run scheduled in at `13:05` Today

## Creating a maven gitlab ci pipeline from scratch

Add the following snippet to your `.gitlab-ci.yml`

```yaml

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  MAVEN_CLI_OPTS: "-Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true --batch-mode -Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS_SKIPTESTS: '-Dcheckstyle.skip=true -DskipTests=true'


#--------------------------------------------------------------------------------
# Build
#--------------------------------------------------------------------------------

maven-build:
  image: maven:latest
  stage: maven-build
  script:
    - mvn package $MAVEN_CLI_OPTS  $MAVEN_CLI_OPTS_SKIPTESTS
    - PROJECT_VERSION=$(mvn $MAVEN_CLI_OPTS  help:evaluate -Dexpression=project.version -q -DforceStdout)
    - echo Project Version is $PROJECT_VERSION
    - echo "PROJECT_VERSION=$PROJECT_VERSION" >> build.env
  artifacts:
    reports:
      dotenv: build.env
    paths:
      - '**/target/*.jar'
      - '**/target/*.war'
    expire_in: 1 week

#--------------------------------------------------------------------------------
# Test
#--------------------------------------------------------------------------------

maven-test:
  image: maven:latest
  stage: maven-test
  script:
    -  mvn test $MAVEN_CLI_OPTS
  artifacts:
    reports:
      junit:
        -  target/site/jacoco/jacoco.xml
```
### Exercise 2
- Something will probably be missing, can you fix it?
  
  _Spoiler_:
  ```bash
  $ echo c3RhZ2VzOgogIC0gbWF2ZW4tYnVpbGQKICAtIG1hdmVuLXRlc3QKICAtIGRlcGxveQ== | base64 -d
  ```
- _Bonus_: Modify the pipeline to not run for merge requests using [workflow](https://docs.gitlab.com/ee/ci/yaml/#workflow) rules
  
  _Spoiler_:
  ```bash
  $ echo d29ya2Zsb3c6CiAgcnVsZXM6CiAgICAtIGlmOiAnJENJX1BJUEVMSU5FX1NPVVJDRSA9PSAibWVyZ2VfcmVxdWVzdF9ldmVudCInCiAgICAgIHdoZW46IG5ldmVyCiAgICAtIHdoZW46IGFsd2F5cw== | base64 -d
  ```
- _Bonus_: Add caching for `.m2/repository`
  
  _Spoiler_:
  ```bash
  $ echo Y2FjaGU6CiAgcGF0aHM6CiAgICAtIC5tMi9yZXBvc2l0b3J5Cg== | base64 -d
  ```


### Bonus Exercise 3

- Add a Gitlab CI Job for building a docker image using [Gitlab's Template](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) as a reference.
  
  _Spoiler_:
   ```bash
   $ echo Iy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCiMgQnVpbGQKIy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCgpidWlsZDoKICBzdGFnZTogZGVwbG95CiAgaW1hZ2U6CiAgICBuYW1lOiBnY3IuaW8va2FuaWtvLXByb2plY3QvZXhlY3V0b3I6ZGVidWcKICAgIGVudHJ5cG9pbnQ6IFsiIl0KICBzY3JpcHQ6CiAgICAtIG1rZGlyIC1wIC9rYW5pa28vLmRvY2tlcgogICAgLSBlY2hvICJ7XCJhdXRoc1wiOntcIiR7Q0lfUkVHSVNUUll9XCI6e1wiYXV0aFwiOlwiJChwcmludGYgIiVzOiVzIiAiJHtDSV9SRUdJU1RSWV9VU0VSfSIgIiR7Q0lfUkVHSVNUUllfUEFTU1dPUkR9IiB8IGJhc2U2NCB8IHRyIC1kICdcbicpXCJ9fX0iID4gL2th bmlrby8uZG9ja2VyL2NvbmZpZy5qc29uCiAgICAtID4tCiAgICAgIC9rYW5pa28vZXhlY3V0b3IKICAgICAgLS1jb250ZXh0ICIke0NJX1BST0pFQ1RfRElSfSIKICAgICAgLS1kb2NrZXJmaWxlICIke0NJX1BST0pFQ1RfRElSfS9Eb2NrZXJmaWxlIgogICAgICAtLWRlc3RpbmF0aW9uICIke0NJX1JFR0lTVFJZX0lNQUdFfToke0NJX0NPTU1JVF9TSEF9Igo= | base64 -d
   ```
- Modify the `kaniko` job to only run for `main` branch and manually triggered using [rules](https://docs.gitlab.com/ee/ci/yaml/#rules):
  
  _Spoiler_:
  ```bash
  $ echo cnVsZXM6CiAgLSBpZjogJyRDSV9DT01NSVRfQlJBTkNIID1+IC9eKG1hc3RlcnxtYWluKSQvJwogICAgd2hlbjogbWFudWFs | base64 -d
  ```
