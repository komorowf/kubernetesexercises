# Challenge - Wordpress Helm

Objective:
* Setup `wordpress` using helm and the `bitnami/wordpress` chart

Requirements:
* Add the bitnami chart repository
* There should be two replicas
* Set resource limits
* Configure the ingress
* Disable all persistence

Optional
* Use the database you created in challenge 01
* Configure Metrics and a [Grafana Dashboard](https://github.com/percona/grafana-dashboards/blob/master/dashboards/MySQL_Overview.json) 

