# Prometheus Grafana and Spring Boot Actuator

[[_TOC_]]

## Deploying a spring boot application with custom metrics
Start by opening the git project [SpringMetricDemo](https://gitlab.com/mt-ag-k8s-training-course/springmetricdemo), then switch to the `prometheus-grafana` branch.
It is not necessary to clone the project, the helm chart is available on your virtual machine.


This project showcases some basic micrometer metrics. First look at the [MetricsController](https://gitlab.com/mt-ag-k8s-training-course/springmetricdemo/-/blob/main/src/main/java/com/mtag/showcases/springboot/MetricsController.java) Class:
```bash
    ...
    
    @Timed("demo_timer")
    @GetMapping("/random")
    public double getRandomMetricsData() throws InterruptedException {
        demoCounter.increment();
        TimeUnit.MILLISECONDS.sleep((long) demoCounter.count()*100);
        return getRandomNumberInRange(0, 100);
    }
    
    ...
```

Each time the `/random` endpoint is called, the call will take 100ms longer then before. Although this is a primitive example, it is not unheard of that some unacceptable growth in execution times, not being caught in automated tests.
Luckily we added some application specific monitoring to our project.

---
In `application.properties`:
```bash
...
management.endpoints.web.exposure.include=health,info,prometheus
...
```

---
In `pom.xml`:
```bash
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
        <dependency>
            <groupId>io.micrometer</groupId>
            <artifactId>micrometer-registry-prometheus</artifactId>
            <scope>runtime</scope>
        </dependency>
        ...
    </dependencies>
```

And finally in our helm chart:
```bash
apiVersion: monitoring.coreos.com/v1 # servicemonitor.yaml
kind: ServiceMonitor
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    release: kube-prometheus-stack
  {{- include "chart.labels" . | nindent 4 }}
spec:
  endpoints:
    - interval: 15s
      path: /actuator/prometheus
      targetPort: 8080
  namespaceSelector:
    matchNames:
      - {{ .Release.Namespace }}
  selector:
    matchLabels:
      {{- include "chart.labels" . | nindent 6 }}
```

### Exercise 1
- Change `!changeme!` in `values-sample.yaml` to use another hostname
- Deploy the helm chart already available on your virtual machine to your namespace
  ```bash
    helm upgrade --install spring-metrics-demo ./chart -f values-sample.yaml
  ```
- Wait a moment and check if all resources are ready `kubectl get pods,ing,svc,cert`
- Call the url from your browser (e.g.  https://!changeme!.mtag-k8s-schulung.site)

## Exploring Prometheus Metrics

- Navigate to the `/random` endpoint, either using the link in the header menu or with https://!changeme!.mtag-k8s-schulung.site/random
- Refresh the site multiple times and watch as the requests are taking longer each time
- Now call the prometheus metrics endpoint at `/actuator/prometheus` - Can you find the custom metrics starting `demo_counter` and `demo_timer`?

```bash
...
demo_counter_total{application="SpringDemo",} 13.0
...
demo_timer_seconds_count{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 13.0
demo_timer_seconds_sum{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 9.187199498
...
demo_timer_seconds_max{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 1.3057129
```

Now head over to [prometheus](https://prometheus.mtag-k8s-schulung.site) and take your time exploring the ui. Check if your application is known to prometheus by navigating to `targets`:
![alt text](./targets.png "Targets")


Head over to graph next and try some simple queries:
![alt text](./graph.png "Graph")

All deployments of this endpoint will provide the same metric names. You can filter a metric for example using your namespace by appending `{namespace="participant-9"}`.
Start by querying the total execution time of the function with:
```
demo_timer_seconds_count{namespace="participant-9"}
```
### Exercise 2

Try to write the following queries.

**Note:** Look at the [cheat sheet](https://promlabs.com/promql-cheat-sheet/) for more examples
- Was the average value of `demo_timer_seconds_max` bigger than `1.0`?
  ```bash
  echo YXZnKGRlbW9fdGltZXJfc2Vjb25kc19tYXh7bmFtZXNwYWNlPSJzY2h1ZWxlcjktbmFtZXNwYWNlIn0pCg== | base64 -d
  ```
- The sum of `demo_counter_total` over all instances and namespaces:
  ```bash
  echo c3VtKGRlbW9fY291bnRlcl90b3RhbCkK | base64 -d
  ```
- (Bonus)  The increase per minute of `demo_counter_total` using the `increase` function
  ```bash
  echo aW5jcmVhc2UoZGVtb19jb3VudGVyX3RvdGFse25hbWVzcGFjZT0ic2NodWVsZXI5LW5hbWVzcGFjZSJ9WzFtXSkK | base64 -d
  ```
- (Bonus) The rate of change of the counter in the last 1m using the `rate` function
  ```bash
  echo cmF0ZShkZW1vX2NvdW50ZXJfdG90YWx7bmFtZXNwYWNlPSJzY2h1ZWxlcjktbmFtZXNwYWNlIn1bMW1dKQo= | base64 -d
  ```
- (Bonus) Prediction of the value of `demo_timer_seconds_max[1h]` in one hour
  ```bash
  echo cHJlZGljdF9saW5lYXIoZGVtb190aW1lcl9zZWNvbmRzX21heHtuYW1lc3BhY2U9InNjaHVlbGVyOS1uYW1lc3BhY2UifVsxaF0sMzYwMCkK | base64 -d
  ```

# Grafana

### Exercise 3

Head over to [grafana](https://grafana.mtag-k8s-schulung.site/) and login using `user:admin password:prom-operator` as credentials

## Create a dashboard

Let's try to create a new dashboard and an alert.

---

![alt text](./create-dashboard.png "Create Dashboard")

Click on `Create->Dashboard` on  the left

---

![alt text](./add-panel.png "Add query")


---

Choose prometheus as the datasource  and select the simple metric `demo_timer_seconds_max{namespace=~"participant-x"}` (of course using your own namespace)

![alt text](./sample-metric.png "Sample Metric")

You can refine queries and explore the available values here as easily as in prometheus itself. Save the planel and dashboard since this is a requirement for creating alerts.

![alt text](./save-panel.png)

---

![img.png](edit-panel.png)

Click on edit-panel and the begin creating an alert.

![img.png](create-alert.png)

Create an alert to trigger when the value is greater than 1.0.

---
![img.png](channel.png)

Finally the labels `channel: msteams` to the alert, then save it. Wait for your alert to appear in the ms teams channel `Alerts`.

---

### Exercise 4

Take your time exploring and try to create a dashboard for our sample application. Nicest dashboard wins. 😉
