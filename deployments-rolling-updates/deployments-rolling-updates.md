# Deployments and Replica Sets

### `create` Deployments

Let's start by creating a deployment. Usually, you will use the yaml files called _manifest_, we will experiment with that later.
But knowing how to use imperative `kubectl` commands can come in quite handy.

```bash
$ kubectl create deployment hello --image=gcr.io/google-samples/hello-app:1.0
```

Let's look at what happened:
```
$ kubectl get pods -w
NAME                       READY   STATUS              RESTARTS   AGE
hello-7fbb7748cc-2fkz6     0/1     ContainerCreating   0          3s
hello-7fbb7748cc-2fkz6     1/1     Running             0          10s
...
```

> **Note**: You can exit `--watch/-w` with `ctrl + c`

---

But of course not only a pod was created:

```
$ kubectl get deployment,replicaset,pod 
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello     1/1     1            1           6m28s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-7fbb7748cc     1         1         1       6m28s

NAME                           READY   STATUS    RESTARTS   AGE
pod/hello-7fbb7748cc-2fkz6     1/1     Running   0          6m29s
```

You can use the `describe` command to describe every object in detail and most importantly view the related events (**Note how the pod name is generated randomly**):

```
$ kubectl describe pod hello
Name:         hello-7fbb7748cc-2fkz6
Namespace:    test
Priority:     0
Node:         aks-npld4v4-12345678-vmss000000/192.168.0.35
Start Time:   Wed, 28 Oct 2020 13:36:47 +0100

...

Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  13m   default-scheduler  Successfully assigned adminer/hello-86c57db685-4lrw8 to aks-agentpool-28126975-vmss000000
  Normal  Pulling    13m   kubelet            Pulling image "hello"
  Normal  Pulled     13m   kubelet            Successfully pulled image "hello"
  Normal  Created    12m   kubelet            Created container hello
  Normal  Started    12m   kubelet            Started container hello
...
```

### Finally remove the deployment again
```
$ kubectl delete deploy hello
deployment.apps "hello" deleted
```

> **Note**: The pod and replicaset that was created was also deleted.

## Creating a deployment using declarative configuration files

To create one or more objects that are declared in a file, just run

```
$ kubectl apply -f echo-simple-deployment.yaml
deployment.apps/echo created
```

Let's look at what was created

```
$ kubectl get deploy,pods,replicaset
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/echo      1/1     1            1           9s

NAME                           READY   STATUS    RESTARTS   AGE
pod/echo-6476cc558f-k2hfn      1/1     Running   0          9s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/echo-6476cc558f      1         1         1       9s
```

---

Now let's talk to our echo service, by port-forwarding _in a separate terminal window_:

```
$ kubectl port-forward deployment/echo 8080:8080
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
Handling connection for 8080
```

> **Note**: Make sure to exit `kubectl port-forward` using `CTRL+C` after finishing

---

Is somebody out there?

```
$ curl localhost:8080
CLIENT VALUES:
client_address=127.0.0.1
command=GET
real path=/
query=nil
request_version=1.1
request_uri=http://localhost:8080/

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=localhost:8080
user-agent=curl/7.68.0
BODY:
```

Now change the value of `replicas: 1` to `replicas: 2` and apply the file again:
```
k get deploy,pods,replicaset
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/echo      1/2     2            1           100s

NAME                           READY   STATUS              RESTARTS   AGE
pod/echo-6476cc558f-2lxr9      0/1     ContainerCreating   0          9s
pod/echo-6476cc558f-k2hfn      1/1     Running             0          100s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/echo-6476cc558f      2         2         1       100s
```

Lets observe what happens, if we kill one of the pods:

```
$ kubectl delete pod echo-<ID>
pod "pods echo-6476cc558f-k2hfn" deleted
```

See how the replicaset made sure that another pod was spawned automatically:
```
$ kubectl get pods
NAME                       READY   STATUS        RESTARTS   AGE
echo-6476cc558f-57ssh      1/1     Running       0          3s
echo-6476cc558f-v7zzv      0/1     Terminating   0          55s
```

You can also scale up or down a deployment using kubectl
```bash
kubectl scale deploy echo  --replicas=4
```

```
$ kubectl get deploy
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
echo      4/4     4            4           10m
```

Scaling down to zero is possible too:
```
$ kubectl scale deploy echo  --replicas=0
```
```
$ kubectl get pods
NAME                       READY   STATUS        RESTARTS   AGE
echo-6476cc558f-4qth6      0/1     Terminating   0          11m
echo-6476cc558f-57ssh      0/1     Terminating   0          10m
echo-6476cc558f-j5nzw      0/1     Terminating   0          105s
```

# Rolling Updates
### `create` Deployment

Create a deployment with multiple replicas
```
kubectl create deployment nginx --image=nginx:latest
kubectl expose deployment nginx --port 80 --type LoadBalancer
kubectl scale deployment nginx --replicas=4
```

---
>**Note**: We will talk about services in detail later. For now knowing that services load balance to multiple pods is enough
---

Note down the loadbalancer IP from the service we just generated (this can take a while)
```
kubectl get service
```

## Watch the server's version and pods
Open another terminal and paste the following command to continously query the image version:
```bash
while true; do date && curl -m 3 -sI  <EXTERNAL-IP>  | grep Server; sleep 1; done
Server: nginx/<version>
Server: nginx/<version>
...
```
You can  continuously watch the pods with the `-w` flag or with
```bash
watch -n 1 kubectl get pods,rs
```
---
>**Note**: You can exit `watch` with `ctrl + c`
---

## Update the deployment

Now change the deployment's image version:
```bash
kubectl set image deployment/nginx nginx=nginx:1.21.5 --record
```

> **Note**: You can specify the --record flag to write the command executed in the resource annotation kubernetes.io/change-cause.
> The recorded change is useful for future introspection. For example, to see the commands executed in each Deployment revision.

---

Watch how the pods are being replaced one by one without causing any outage.

You can watch the rollout status using:
```
$ kubectl rollout status deployment nginx
deployment "nginx" successfully rolled out
```
The rollout history is accessible using:

```
$ kubectl rollout history deployment nginx
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl set image deployment/nginx nginx=nginx:1.21.5 --record=true
```

We can inspect the revision in more detail with
```bash
kubectl rollout history deploy/nginx --revision 2
```

Next, let's try out what happens in case of a failing deployment.

```
$ kubectl set image deployment/nginx nginx=nginx:na --record=true
```

We can undo this using:
```
$ kubectl rollout undo deployment nginx
```
## Export a resource to yaml
Save the current state of the deployment into a yaml file to make editing easier. 
```bash
kubectl get deployment nginx -o yaml > deployment.yaml
```

## StrategyType: Recreate
Change the field 
```
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
```
to
```
 strategy:
    type: Recreate
```

Now reapply the failed update and observe what happens:

```bash
kubectl replace -f deployment.yaml
```

> **Note**: The Strategy `Recreate` can be useful in debug environments, but is not recommended otherwise.

---

## Replicasets

Do you remember how Deployments perform rolling updates?
Lets check the ReplicaSets that have been created in the meantime.

```bash
kubectl get replicaset
```
